﻿namespace InvalidArgSample
{
    using EnvDTE;
    using EnvDTE80;
    using Microsoft.VisualStudio.Shell;
    using Microsoft.VisualStudio.Threading;
    using System.Diagnostics.CodeAnalysis;
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for ToolWindow1Control.
    /// </summary>
    public partial class ToolWindow1Control : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ToolWindow1Control"/> class.
        /// </summary>
        public ToolWindow1Control()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Handles click on the button by displaying a message box.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event args.</param>
        [SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions", Justification = "Sample code")]
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Default event handler naming pattern")]
        private void button1_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(
                string.Format(System.Globalization.CultureInfo.CurrentUICulture, "Invoked '{0}'", this.ToString()),
                "ToolWindow1");
        }

        private DTE2 GetDTE()
        {
            return (EnvDTE80.DTE2)Package.GetGlobalService(typeof(EnvDTE.DTE));
        }

        private Project GetTopLevelFolder()
        {
            Dispatcher.VerifyAccess();

            Solution2 sln = (Solution2)GetDTE().Solution;
            var targetName = "TopLevelFolder";
            Project prj = null;

            for (var i = 1; i <= sln.Count && (prj = sln.Item(i)).Name != targetName; i++) ;

            if (prj == null)
            {
                throw new System.Exception("Couldn't find top-level project element named '" + targetName + "'");
            }

            return prj;
        }

        private async void btnDeleteTopLevel_Click(object sender, RoutedEventArgs e)
        {
            await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync();

            Project prj = null;
            try
            {
                prj = GetTopLevelFolder();
            }
            catch (System.Exception exception)
            {
                MessageBox.Show(exception.Message);
                return;
            }

            prj.Delete();
        }

        private async void btnDeleteSubItems_Click(object sender, RoutedEventArgs e)
        {
            await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync();

            Project prj = null;
            try
            {
                prj = GetTopLevelFolder();
            }
            catch (System.Exception exception)
            {
                MessageBox.Show(exception.Message);
                return;
            }

            while (prj.ProjectItems.Count > 0)
            {
                var item = prj.ProjectItems.Item(1);
                item.Remove();
            }
        }
    }
}